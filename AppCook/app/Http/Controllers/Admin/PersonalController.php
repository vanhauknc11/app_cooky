<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
class PersonalController extends Controller
{
    //
    public function getproductlist($id,Request $request){

      
        try{
                 $limit=10;
                 if( !\is_null ($request->limit)){
                     $limit =$request->limit;
                 }
                $orders = [];
                $orders['column'] = 'id';
                $orders['sort'] = 'desc';
                    

            $query = \DB::table('tb_product')->where('author','=',$id)->orderBy('id','desc')->paginate($limit);
            
    
          return  \response()->json([
                'status'=>true,
                'message'=>'OK',
                'data'=>$query->items(),
              
                'meta'=>[
                    'total'=>$query->total(),
                    'perPage'=>$query->perPage(),
                    'currentPage' => $query->currentPage()
                ]
                ],200);

        }catch(\Exception $e){
             return   \response()->json([
                    'status'=>false,
                    'message'=>$e->getMessage(),
             ],500);
        }



    }
}
