<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Type;

use Validator;
use App\Service\ProductService;
use Symfony\Component\HttpFoundation\Response;
class ProductController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $productService;
    
    public function __construct(ProductService $productservice){
       
        $this->productService = $productservice;
    }

    public function index(Request $request)
    {
        //
        try{
           
            $orders = [];
            if($request->get('column')&& $request->get('sort')){
                $orders['column'] = $request->get('column');
                $orders['sort'] = $request->get('sort');
                    
                
            }
            $products = $this->productService->getAll($orders,$request->get('limit') ?? config('app.paginate.per_page') );
            
          return  \response()->json([
                'status'=>true,
                'message'=>'OK',
                'data'=>$products->items(),
              
                'meta'=>[
                    'total'=>$products->total(),
                    'perPage'=>$products->perPage(),
                    'currentPage' => $products->currentPage()
                ]
                ],200);

        }catch(\Exception $e){
             return   \response()->json([
                    'status'=>false,
                    'code'=>Response::HTTP_INTERNAL_SERVER_ERROR,
                    'message'=>$e->getMessage(),
             ],500);
        }
  //      return response()->json(Product::all(),200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        



       
        $rules = [
            'title' => 'required',
            'content' => 'required',
            'idtype' => 'required',
            'author'=> 'required'
        ];    

        $validator = Validator::make($request->all(),$rules);
        try{

        if($validator->fails()){
            return response()->json([
                'status'=>false,
                'message'=>$validator->errors()
            ],200);
        }

        if(! $request->file('img')){
            return \response()->json([
                'status'=>false,
                'message'=>'Error No Image'
                ],200);
        }

     
     

 
       
        //dd($request->file('img'));
        $filename = rand(0,100000).$request->file('img')->getClientOriginalName();
        $path = $request->file('img')->move(public_path("/image"),$filename);
        $photoUrl = url('/image/'.$filename);

        

        $type = Type::find($request->idtype);
        if(\is_null($type)){
            return \response()->json([               
                'status'=>false,
                "message"=>"Error Type"],200);
        }

        $product = new Product();
        $product->title=$request->title;
        $product->img=$photoUrl;
        $product->content=$request->content;
        $product->idtype=$request->idtype;
        $product->author=$request->author;
        $product->resources=$request->resources;
        $product->tutorial=$request->tutorial;
        $product->save();
       
        return \response()->json([
            'status'=>true,
            "message"=>"OK",
            'data'=>$product
        ],200);

         }catch(\Exception $e){
            return   \response()->json([
                'status'=>false,
                'message'=>$e->getMessage()
                    
            ],200);

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $product = Product::find($id);
        if(!is_null($product)){
            $user = \DB::table('users')->where('id', $product->author)->first();
            $product['name']=$user->name;
           // dd($user->name);
            return \response()->json([
                'status'=>true,
                'message'=>'OK',
                'data'=>$product,
                
                
            ],200);
        }else{
            return \response()->json([
                'status'=>false,
                "message"=>"Record not found !"
                ],200);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        try{
            
        $product = Product::find($id);
        if(!is_null($product)){
            if(( $request->hasFile('img') ))
            {
                $filename = rand(0,100000).$request->file('img')->getClientOriginalName();
             $path = $request->file('img')->move(public_path("/image"),$filename);
              $photoUrl = url('/image/'.$filename);
              $product->img=$photoUrl;
              $request->img=$photoUrl;

            }


            if($request->title != null){
                $product->title=$request->title;
            }
            if($request->content != null){
                $product->content=$request->content;
            }
            if($request->resources != null){
                $product->resources=$request->resources;
            }
            if($request->tutorial != null){
                $product->tutorial=$request->tutorial;
            }
            if($request->idtype != null){
                $product->idtype=$request->idtype;
            }
            
           // dd($request->img);
            $arr=[
            'title'=>$product->title,
            'img'=>$product->img,
            'content'=>$product->content,
            'resources'=>$product->resources,
            'idtype'=>$product->idtype,
            'tutorial'=>$product->tutorial
            
            ];
            
            $product->update($arr);
            return \response()->json([
                'status'=>true,
                'message'=>"OK",
                'data'=>$product
            ],200);
        }else{
            return \response()->json(
                [
                'status'=>false,
                "message"=>"Record not found !"
                ],200);
        }

        }catch(Exception $e){

            return   \response()->json([
                'status'=>false,
                'message'=>$e->getMessage()
               
            ],Response::HTTP_INTERNAL_SERVER_ERROR);

        }
   


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $product = Product::find($id);
        if(!is_null($product)){
            $product->delete();
            return \response()->json([
                'status'=>true,
                'message'=>'Deleted',
            ],200);
        }else{
            return \response()->json(
               [ "status"=>false,
               "message"=>"Record not found !"
                ],200);
        }

    }
}
