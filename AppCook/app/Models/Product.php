<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
   protected $table = "TB_Product";

   protected $fillable =[
       'title',
       'img',
       'content',
       'idtype',
       'author',
       'resources',
       'tutorial'

   ];


}
