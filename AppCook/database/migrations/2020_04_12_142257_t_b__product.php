<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TBProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('TB_Type', function (Blueprint $table) {
          
            $table->increments('id',true);
            $table->string('nametype')->unsigned();
            
        });

        Schema::create('TB_Product', function (Blueprint $table) {
          
            $table->increments('id',true);
            $table->string('title')->unsigned();
            $table->string('img')->unsigned();
            $table->string('content')->unsigned();
            $table->integer('idtype')->unsigned();
            $table->timestamps();
        });

        Schema::connection('mysql')->table('TB_Product', function (Blueprint $table) {
            $table->foreign('idtype')->references('id')->on('TB_Type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('TB_Product');
        Schema::drop('TB_Type');
    }
}
