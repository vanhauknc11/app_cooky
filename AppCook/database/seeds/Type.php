<?php

use Illuminate\Database\Seeder;

class Type extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('TB_type')->insert([
            [
            'nametype' => 'Bua Sang',
        ],
         [
        'nametype' => 'Bua Trua',
        ],
        [
        'nametype' => 'Bua Toi',
        ]
        ]
    );
    }
}
