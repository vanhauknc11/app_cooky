<!DOCTYPE html>
<html lang="zxx">

<head>
<base href="{{asset('../public/client')}}/"/>
    <meta charset="UTF-8">
    <meta name="description" content="Ogani Template">
    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cook</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Humberger Begin -->
    <div class="humberger__menu__wrapper">
        <div class="humberger__menu__logo">
            <a href="#"><img src="img/logo.png" alt=""></a>
        </div>
        <nav class="humberger__menu__nav mobile-menu">
            <ul>
                <li class="active"><a href="./index.html">Home</a></li>
                <li><a href="./shop-grid.html">My Posts</a></li>
                <li><a href="#" id="name">Pages</a>
                    <ul class="header__menu__dropdown">
                        <li><a href="#" id="email">Shop Details</a></li>
                        <li><a href="login.html">Sign Out</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div class="header__top__right__social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest-p"></i></a>
        </div>
    </div>
    <!-- Humberger End -->

    <!-- Header Section Begin -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="./index.html"><img src="img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu">
                        <ul>
                            <li><a href="{{asset('home')}}">Home</a></li>
                            <li class="active"><a href="{{asset('mypost')}}">My Posts</a></li>
                            <li>
                                <a id="myname"></a>
                                <ul class="header__menu__dropdown">
                                    <li>
                                        <a id="myemail"></a>
                                    </li>
                                    <li><a href="{{asset('./')}}">Sign Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>
    <!-- Header Section End -->

    <!-- Hero Section Begin -->
    <section class="hero">
        <div class="container">
            <div class="row">
                <div class="col-lg-1">

                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form action="#">
                                <input type="text" placeholder="What do you need?">
                                <button type="submit" class="site-btn">SEARCH</button>
                            </form>
                        </div>
                        <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5>0988 808 999</h5>
                                <span>support 24/7 time</span>
                            </div>
                        </div>
                    </div>
                    <div class="hero__item set-bg" data-setbg="img/hero/banner.jpg">
                        <div class="hero__text">
                            <span>CREATE YOUR DISH</span>
                            <h2>Show Me <br />Your Skills</h2>
                            <p>Share with everyone in the world your dish</p>
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                data-target="#myModal"><a>UPLOAD YOUR POST</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->
    <!-- Post Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background-color:#7fad39;">
                    <h4 style="color: white; margin-left: auto;"><b>UPLOAD YOUR POST</b></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form id="data" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <b>Title</b>
                            <input class="form-control form-control-lm" type="text" placeholder="Dish name" name="title"
                                required>
                            <br>
                            <b>Content</b>
                            <input class="form-control form-control-lm" type="text" placeholder="Dish recommendation"
                                name="content" required>
                            <br>
                            <b>Materials</b>
                            <input class="form-control form-control-lm" type="text" placeholder="Cook materials"
                                name="resources" required>
                            <br>
                            <b>Recipe</b>
                            <textarea class="form-control" rows="5" name="tutorial" placeholder="Cook recipe"
                                required></textarea>
                            <br>
                            <b>Image</b>
                            <input class="form-control form-control-lm" type="file" name="img" required>
                            <br>
                            <b>Type</b>
                            <br>
                            <select name="idtype">
                                <option value="1">Ăn sáng</option>
                                <option value="2">Ăn vặt</option>
                                <option value="3">Khai vị</option>
                                <option value="4">Món chay</option>
                                <option value="6">Món chính</option>
                                <option value="7">Lẩu</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn-info btn-sm" id="insert">
                            <b>Upload</b></button>
                        </b>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background-color:#7fad39;">
                    <h4 style="color: white; margin-left: auto;"><b>EDIT YOUR POST</b></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form id="dataEdit" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <b>Title</b>
                            <input class="form-control form-control-lm" type="text" placeholder="Dish name" name="title"
                                id="title" required>
                            <br>
                            <b>Content</b>
                            <input class="form-control form-control-lm" type="text" placeholder="Dish recommendation"
                                name="content" id="content" required>
                            <br>
                            <b>Materials</b>
                            <input class="form-control form-control-lm" type="text" placeholder="Cook materials"
                                name="resources" id="resources" required>
                            <br>
                            <b>Recipe</b>
                            <textarea class="form-control" rows="5" name="tutorial" placeholder="Cook recipe"
                                id="tutorial" required></textarea>
                            <br>
                            <b>Image</b>
                            <input class="form-control form-control-lm" type="file" name="img">
                            <br>
                            <b>Type</b>
                            <br>
                            <select name="idtype" id="idtype">
                                <option value="1">Bữa sáng</option>
                                <option value="2">Bữa trưa</option>
                                <option value="3">Bữa tối</option>
                                <option value="4">Bữa khuya</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn-info btn-sm" id="edit">
                            <b>Update</b></button>
                        </b>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete Post -->
    <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: red; padding: 15px 0; height: 50px;">
                    <h5 style="margin-left: 60px; color: white;"><b>Bạn chắc chắn muốn
                            xoá món ăn này?</b></h5>
                </div>
                <div class="modal-body" style="padding: 10px 0; margin-left: 170px;">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" id="delete">Delete</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal"
                        style="margin-left: 20px;">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Featured Section Begin -->
    <section class="featured spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Your Posts</h2>
                    </div>
                </div>
            </div>
            <div class="row posts featured__filter">

            </div>
        </div>
    </section>
    <!-- Featured Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__about__logo">
                            <a href="./index.html"><img src="img/logo.png" alt=""></a>
                        </div>
                        <ul>
                            <li>Address: 97 Man Thien, Hiep Phu Ward, District 9</li>
                            <li>Phone: 0988808999</li>
                            <li>Email: nhatduy.education@gmail.com</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                    <div class="footer__widget">
                        <h6>Useful Links</h6>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">About Our Shop</a></li>
                            <li><a href="#">Secure Shopping</a></li>
                            <li><a href="#">Delivery infomation</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Our Sitemap</a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Who We Are</a></li>
                            <li><a href="#">Our Services</a></li>
                            <li><a href="#">Projects</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Innovation</a></li>
                            <li><a href="#">Testimonials</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="footer__widget">
                        <h6>Our Social</h6>
                        <div class="footer__widget__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/myposts.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js">
    </script>
</body>

</html>