<!DOCTYPE html>
<html lang="en">

<head>
<base href="{{asset('../public/client')}}/"/>
    <title>Sign Up</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css" />
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
    <!--===============================================================================================-->
</head>

<body>
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form class="login100-form validate-form">
                    <span class="login100-form-title p-b-34">
                        Sign Up
                    </span>
                    <div class="wrap-input100 rs3-wrap-input100 validate-input m-b-10" data-validate="Type your name">
                        <input id="name" class="input100" type="text" placeholder="Your Name" autocomplete="off" />
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 rs3-wrap-input100 validate-input m-b-10" data-validate="Type email">
                        <input id="email" class="input100" type="email" placeholder="Email" autocomplete="off" />
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 rs3-wrap-input100 validate-input m-b-10" data-validate="Type password">
                        <input id="password" class="input100" type="password" placeholder="Password" />
                        <span class="focus-input100"></span>
                    </div>
                    <div class="wrap-input100 rs3-wrap-input100 validate-input m-b-20"
                        data-validate="Type password again">
                        <input id="passagain" class="input100" type="password" placeholder="Password Again" />
                        <span class="focus-input100"></span>
                    </div>

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn" id="signup">
                            Sign up
                        </button>
                    </div>

                    <div class="w-full text-center p-t-34">
                        <a href="{{asset('/')}}" class="txt3">
                            Sign In
                        </a>
                    </div>
                </form>

                <div class="login100-more" style="background-image: url('images/login.jpg');"></div>
            </div>
        </div>
    </div>

    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/daterangepicker/moment.min.js"></script>
    <script src="vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/countdowntime/countdowntime.js"></script>

    <script src="js/signup.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js">
    </script>
</body>

</html>