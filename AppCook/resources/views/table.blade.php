                                        
       @extends('master')
       @section('title','Quản lý người dùng')
       @section('main')                                 
                                        <div class="user-data m-b-30" onload="test()">
                                    <h3 class="title-3 m-b-30">
                                        <i class="zmdi zmdi-account-calendar"></i>user data</h3>
                                    <div class="filters m-b-45">
                                        <div class="rs-select2--dark rs-select2--md m-r-10 rs-select2--border">
                                            <select class="js-select2" name="property">
                                                <option selected="selected">All Properties</option>
                                                <option value="">Products</option>
                                                <option value="">Services</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <div class="rs-select2--dark rs-select2--sm rs-select2--border">
                                            <select class="js-select2 au-select-dark" name="time">
                                                <option selected="selected">All Time</option>
                                                <option value="">By Month</option>
                                                <option value="">By Day</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                    </div>
                                    <div class="table-responsive table-data">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        <label class="au-checkbox">
                                                            <input type="checkbox">
                                                            <span class="au-checkmark"></span>
                                                        </label>
                                                    </td>
                                                    <td>name</td>
                                                    <td>role</td>
                                                    <td>custom</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                  
                                </div>
                                            <!-- MODAL -->
                                <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="mediumModalLabel">Medium Modal</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                

                                         
                                <div class="card">
                                    <div class="card-header">Example Form</div>
                                    <div class="card-body card-block">
                                        <form action="" method="post" class="">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-user"></i>
                                                    </div>
                                                    <input type="text" id="name" name="name" placeholder="NGUYEN VAN HAU" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-envelope"></i>
                                                    </div>
                                                    <input type="email" id="email" name="email" placeholder="Email" class="form-control" readonly="true">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-asterisk"></i>
                                                    </div>
                                                    <input type="password" id="oldpassword" name="oldpassword" placeholder="Old Password" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-asterisk"></i>
                                                    </div>
                                                    <input type="password" id="password" name="password" placeholder="Password" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-actions form-group">
                                                <button type="submit" class="btn btn-success btn-sm">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                     


                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                <button type="button" class="btn btn-primary">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                                <script>
                           document.getElementsByTagName("BODY")[0].onload = function() {getinfouser()};

                                function getinfouser(){
                                    $.ajax({
                                            type: "GET",
                                            url: 'http://hyperzone.xyz/AppCook/api/user',
                                            data: $(this).serialize(),
                                            success: function(data)
                                                {
                                                   
                                            //    console.log(data);
                                            //         alert('Invalid Credentials');

                                            for(var i=0;i<data.data.length;i++){

                                                var srole='user';
                                                if(data.data[i].role === 'admin'){
                                                    srole='admin';
                                                }


                                            $('tbody').append(  '   <tr>  '  + 
'                                                       <td>  '  + 
'                                                           <label class="au-checkbox">  '  + 
'                                                               <input type="checkbox">  '  + 
'                                                               <span class="au-checkmark"></span>  '  + 
'                                                           </label>  '  + 
'                                                       </td>  '  + 
'                                                       <td>  '  + 
'                                                           <div class="table-data__info">  '  + 
'                                                               <h6>'+data.data[i].name+'</h6>  '  + 
'                                                               <span>  '  + 
'                                                                   <a href="#">'+data.data[i].email+'</a>  '  + 
'                                                               </span>  '  + 
'                                                           </div>  '  + 
'                                                       </td>  '  + 
'                                                       <td>  '  + 
'                                                           <span class="role '+srole+'">'+srole+'</span>  '  + 
'                                                       </td>  '  + 
'     '  + 
'     '  + 
'                                                       <td>  '  + 
'     '  + 
'                                                       <div class="table-data-feature">  '  + 
'                                                           <button class="item" data-toggle="tooltip" data-placement="top" title="Send">  '  + 
'                                                               <i class="zmdi zmdi-mail-send"></i>  '  + 
'                                                           </button>  '  + 
'                                                           <button class="item" data-toggle="tooltip" data-placement="top"  title="Edit" value="edit/'+data.data[i].id+'">  '  + 
'                                                               <i class="zmdi zmdi-edit"></i>  '  + 
'                                                           </button>  '  + 
'                                                           <button class="item" data-toggle="tooltip" data-placement="top" title="Delete" value="delete/'+data.data[i].id+'"">  '  + 
'                                                               <i class="zmdi zmdi-delete"></i>  '  + 
'                                                           </button>  '  + 
'                                                            '  + 
'                                                       </div>  '  + 
'                                                   </td>  '  + 
'     '  + 
'                                                  </tr>  ' );

}


                                                }
                                            
                                        });

                                    }
        
                                  
                                </script>


                                <script>

                            $('tbody').on('click', 'button', function(){
                               var x =  $(this).val();
                              var ar =  x.split('/');
                              
                               if(ar[0]=='edit'){
                                console.log('editt/'+ar[1]);
                               }else if(ar[0]=='delete'){

                               // var r = confirm('Bạn có chắc chắn xóa !');


                                Swal.fire({
                                    title: 'Are you sure?',
                                    text: "You won't be able to revert this!",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Yes, delete it!'
                                    }).then((result) => {
                                    if (result.value) {

                                        $.ajax({
                                            type: "DELETE",
                                            url: 'http://hyperzone.xyz/AppCook/api/user/'+ar[1]+'',
                                            success: function(data)
                                                { 
                                                    if(data.status==true){
                                                     Swal.fire(
                                                        'Deleted!',
                                                        'Your account has been deleted.',
                                                        'success'
                                                        );
                                                    }else{
                                                        Swal.fire({
                                                        title: 'Error !',
                                                        text: 'Error !',
                                                        icon: 'error',
                                                        confirmButtonText: 'OK'
                                                        });
                                                    }

                                                }   
                                            });


                                       
                                    }
                                    })

                                // if (r == true) {
                                   
                                //     $.ajax({
                                //             type: "DELETE",
                                //             url: 'http://hyperzone.xyz/AppCook/api/user/'+ar[1]+'',
                                //             success: function(data)
                                //                 { 
                                //                     if(data.status==true){
                                //                         console.log('delete thanh cong');
                                //                     }else{
                                //                         console.log(data.mesage);
                                //                     }

                                //                 }   
                                //             });
                                   
                                //     } 

                               
                                 
                               }
                               
                            });


                           

                                   
                                       
   
                                </script>
        
        @stop