
@extends('master')
@section('title','Quản lý Data')

@section('main')
                    <div class="cach" style="margin-top:100px">
                    <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">data table</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            <select class="js-select2" name="property">
                                                <option selected="selected">All Properties</option>
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <div class="rs-select2--light rs-select2--sm">
                                            <select class="js-select2" name="time">
                                                <option selected="selected">Today</option>
                                                <option value="">3 Days</option>
                                                <option value="">1 Week</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <button class="au-btn-filter">
                                            <i class="zmdi zmdi-filter-list"></i>filters</button>
                                    </div>
                                    <div class="table-data__tool-right">
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>add item</button>
                                        <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                            <select class="js-select2" name="type">
                                                <option selected="selected">Export</option>
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </th>
                                                <th>Author</th>
                                                <th>name</th>
                                                <th>description</th>
                                                <th>date</th>
                                                <th>img</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                           
                                           
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                        </div>

                                <div class="user-data__footer">
                                        <button id="btnload" class="au-btn au-btn-load">load more</button>
                                    </div>


                <!-- DAY LA MODAL -->


                            <!-- MODAL -->

                            <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="mediumModalLabel">Medium Modal</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                

                                         
                                            
                                <div class="card" id="xmodal">
                                  
                               
                                </div>
                                

                                  
                                    </div>
                                </div>
                            </div>  
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>


                <!-- HET MODAL -->




                <script>

                    document.getElementsByTagName("BODY")[0].onload = function() {getinfoproduct()};

                    function getinfoproduct(){
                        $.ajax({
                                type: "GET",
                                url: 'http://hyperzone.xyz/AppCook/api/product?page=1&limit=5&column=id&sort=desc',
                                data: $(this).serialize(),
                                success: function(data)
                                    {
                                       
                                        for(var i=0;i<data.data.length;i++){
                                            var text="";
                                            if(data.data[i].content.length>20){
                                                 text = data.data[i].content.substring(0,15)+"....";
                                            }else{
                                                text = data.data[i].content;
                                            }
                                            
                                        $('tbody').append( '    <tr class="tr-shadow">  '  + 
 '                                                   <td>  '  + 
 '                                                       <label class="au-checkbox">  '  + 
 '                                                           <input type="checkbox">  '  + 
 '                                                           <span class="au-checkmark"></span>  '  + 
 '                                                       </label>  '  + 
 '                                                   </td>  '  + 
 '                                                   <td>'+data.data[i].name+'</td>  '  + 
 '                                                   <td>  '  + 
 '                                                       <span class="block-email">'+data.data[i].title+'</span>  '  + 
 '                                                   </td>  '  + 
 '                                                   <td class="desc">'+text+'</td>  '  + 
 '                                                   <td>'+data.data[i].created_at+'</td>  '  + 
 '                                                   <td>  '  + 
 '                                                       <span class="status--process">  '  + 
 '                                                       <img src="'+data.data[i].img+'" alt="" style="width:50px">  '  + 
 '                                                       </span>  '  + 
 '                                                   </td>  '  + 
 '                                                 '  + 
 '                                                   <td>  '  + 
 '                                                       <div class="table-data-feature">  '  + 
 '                                                           <button class="item" data-toggle="tooltip" data-placement="top" title="Send">  '  + 
 '                                                               <i class="zmdi zmdi-mail-send"></i>  '  + 
 '                                                           </button>  '  + 
 '                                                           <button class="item" data-toggle="modal" data-target="#mediumModal" data-placement="top" title="Edit" value="edit/'+data.data[i].id+'">  '  + 
 '                                                               <i class="zmdi zmdi-edit"></i>  '  + 
 '                                                           </button>  '  + 
 '                                                           <button class="item" data-toggle="tooltip" data-placement="top" title="Delete" value="delete/'+data.data[i].id+'">  '  + 
 '                                                               <i class="zmdi zmdi-delete"></i>  '  + 
 '                                                           </button>  '  + 
 '                                                           <button class="item" data-toggle="tooltip" data-placement="top" title="More">  '  + 
 '                                                               <i class="zmdi zmdi-more"></i>  '  + 
 '                                                           </button>  '  + 
 '                                                       </div>  '  + 
 '                                                   </td>  '  + 
 '                                               </tr>  '  + 
 '                                              <tr class="spacer"></tr>  '  );
                                        }
                                    }
                        });
                    }



                    $('tbody').on('click', 'button', function(){
                               var x =  $(this).val();
                              
                               console.log(x);
                                var ar = x.split('/');
                                if(ar[0]=='edit'){

                                    $.ajax({
                                            type: "GET",
                                            url: 'http://hyperzone.xyz/AppCook/api/product/'+ar[1]+'',
                                            
                                            success: function(data)
                                                { 
                                                    
                                                    if(data.status==true){

                                                        $('#xmodal').html( '     <div class="card-header">  '  + 
 '                                           <strong>Basic Form</strong> Elements  '  + 
 '                                       </div>  '  + 
 '                                       <div class="card-body card-block">  '  + 
 '                                           <form action="" method="post"  id="formedit" class="form-horizontal">  '  + 
 '                                                 '  + 
 '                                               <div class="row form-group">  '  + 
 '                                                   <div class="col col-md-3">  '  + 
 '                                                       <label for="text-input" class=" form-control-label">ID</label>  '  + 
 '                                                   </div>  '  + 
 '                                                   <div class="col-12 col-md-9">  '  + 
 '                                                       <input type="text" id="id" name="id"  value="'+data.data.id+'" class="form-control" readonly="true">  '  + 
 '                                                       <!-- <small class="form-text text-muted">This is a help text</small> -->  '  + 
 '                                                   </div>  '  + 
 '                                               </div>  '  + 
 '                                               <div class="row form-group">  '  + 
 '                                                   <div class="col col-md-3">  '  + 
 '                                                       <label for="email-input" class=" form-control-label">Title</label>  '  + 
 '                                                   </div>  '  + 
 '                                                   <div class="col-12 col-md-9">  '  + 
 '                                                       <input type="" id="title" name="title"  value="'+data.data.title+'" class="form-control">  '  + 
 '                                                       <small class="help-block form-text">Title of dish</small>  '  + 
 '                                                   </div>  '  + 
 '                                               </div>  '  + 
 '     '  + 
 '                                               <div class="row form-group">  '  + 
 '                                                   <div class="col col-md-3">  '  + 
 '                                                       <label for="textarea-input" class=" form-control-label">Content</label>  '  + 
 '                                                   </div>  '  + 
 '                                                   <div class="col-12 col-md-9">  '  + 
 '                                                       <textarea name="content" id="content" rows="5"  class="form-control">'+data.data.content+'</textarea>  '  + 
 '                                                   </div>  '  + 
 '                                               </div>  '  + 
 '     '  + 
 '                                               <div class="row form-group">  '  + 
 '                                                   <div class="col col-md-3">  '  + 
 '                                                       <label for="textarea-input" class=" form-control-label">Resources</label>  '  + 
 '                                                   </div>  '  + 
 '                                                   <div class="col-12 col-md-9">  '  + 
 '                                                       <textarea name="resources" id="resources" rows="4" placeholder="resources..." class="form-control">'+data.data.resources+'</textarea>  '  + 
 '                                                   </div>  '  + 
 '                                               </div>  '  + 
 '     '  + 
 '                                                 '  + 
 '                                                 '  + 
 '                                                 '  + 
 '                                               <div class="row form-group">  '  + 
 '                                                   <div class="col col-md-3">  '  + 
 '                                                       <label for="textarea-input" class=" form-control-label">Tutorial</label>  '  + 
 '                                                   </div>  '  + 
 '                                                   <div class="col-12 col-md-9">  '  + 
 '                                                       <textarea name="tutorial" id="tutorial" rows="4" placeholder="tutorial..." class="form-control">'+data.data.tutorial+'</textarea>  '  + 
 '                                                   </div>  '  + 
 '                                               </div>  '  + 
 '     '  + 
 '                                               '  + 
 '                                                 '  + 
 '                                                 '  + 
 '                                               '  + 
 '                                                 '  + 
 '                                               <div class="row form-group">  '  + 
 '                                                   <div class="col col-md-3">  '  + 
 '                                                       <label for="file-input" class=" form-control-label">File input</label>  '  + 
 '                                                   </div>  '  + 
 '                                                   <div class="col-12 col-md-9">  '  + 
 '                                                       <input type="file" id="img" name="img" class="form-control-file">  '  + 
 '                                                   </div>  '  + 
 '                                               </div>  '  + 
 '                                            '  + 
 '                                       <div class="card-footer">  '  + 
 '                                           <button type="submit" class="btn btn-primary btn-sm">  '  + 
 '                                               <i class="fa fa-dot-circle-o"></i> Submit  '  + 
 '                                           </button>  '  + 
 '                                           <button type="reset" class="btn btn-danger btn-sm">  '  + 
 '                                               <i class="fa fa-ban"></i> Reset  '  + 
 '                                           </button>  '  + 
 '                                       </div>  '  + 
 '                                       </form>  '  + 
 '                                      </div>  ' );


                                                    }else{
                                                        console.log("Khong ton tai");
                                                    }

                                                }   
                                            });
                                    
                                }else{
                                    if(ar[0]=='delete'){



                                     Swal.fire({
                                    title: 'Are you sure?',
                                    text: "You won't be able to revert this!",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Yes, delete it!'
                                    }).then((result) => {
                                    if (result.value) {

                                        $.ajax({
                                            type: "DELETE",
                                            url: 'http://hyperzone.xyz/AppCook/api/product/'+ar[1]+'',
                                            success: function(data)
                                                { 
                                                    if(data.status==true){
                                                        Swal.fire({
                                                        title: 'Success !',
                                                        text: 'Delete Success !',
                                                        icon: 'success',
                                                        confirmButtonText: 'OK'
                                                        });
                                                    }else{
                                                        Swal.fire({
                                                        title: 'Error !',
                                                        text: 'Error !',
                                                        icon: 'error',
                                                        confirmButtonText: 'OK'
                                                        });
                                                    }

                                                }   
                                            });
                                       
                                    }
                                    })

                                   



                                   
                                  
                                   
                                

                                    }
                                   
                                }
                               
                               
                            });



                         

                            $('#xmodal').on('submit', '#formedit', function(e){

                                
                                e.preventDefault();
                               
                                var formData = new FormData(this);
                               
                           
                               // console.log(formData);
                                var id = $('#id').val();
                              
                                    $.ajax({
                                    type: "POST",
                                    url: 'http://hyperzone.xyz/AppCook/api/product/'+id+'?_method=PUT',
                                    data: formData,
                                    cache:false,
                                    contentType: false,
                                    processData: false,
                                    success: function(data)
                                        {
                                            //console.log(data);
                                        if (data.status === true) {
                                            Swal.fire({
                                            title: 'Success !',
                                            text: 'Cập nhật thành công !',
                                            icon: 'success',
                                            confirmButtonText: 'OK'
                                            });
                                            
                                        }
                                        else {
                                            Swal.fire({
                                            title: 'Error !',
                                            text: 'Có lỗi xảy ra. Vui lòng thử lại !',
                                            icon: 'error',
                                            confirmButtonText: 'OK'
                                            });
                                            console.log("loi");
                                    //         alert('Invalid Credentials');
                                        }
                                    }
                                });
                            });

                    var a=0;
                    $('#btnload').click(function(){
                        a=a+1;
                      //  console.log(a);
                        $.ajax({
                            
                            type: "GET",
                            url: 'http://hyperzone.xyz/AppCook/api/product?page='+a+'&limit=5&column=id&sort=desc',
                            data: $(this).serialize(),
                            success: function(data)
                                {
                                    
                                if (data.status == true ) {
                                    console.log("ok");
                                    console.log(data);

                                    for(var i=0;i<data.data.length;i++){
                                            var text="";
                                            if(data.data[i].content.length>20){
                                                 text = data.data[i].content.substring(0,15)+"....";
                                            }else{
                                                text = data.data[i].content;
                                            }
                                            
                                        $('tbody').append( '    <tr class="tr-shadow">  '  + 
 '                                                   <td>  '  + 
 '                                                       <label class="au-checkbox">  '  + 
 '                                                           <input type="checkbox">  '  + 
 '                                                           <span class="au-checkmark"></span>  '  + 
 '                                                       </label>  '  + 
 '                                                   </td>  '  + 
 '                                                   <td>'+data.data[i].name+'</td>  '  + 
 '                                                   <td>  '  + 
 '                                                       <span class="block-email">'+data.data[i].title+'</span>  '  + 
 '                                                   </td>  '  + 
 '                                                   <td class="desc">'+text+'</td>  '  + 
 '                                                   <td>'+data.data[i].created_at+'</td>  '  + 
 '                                                   <td>  '  + 
 '                                                       <span class="status--process">  '  + 
 '                                                       <img src="'+data.data[i].img+'" alt="" style="width:50px">  '  + 
 '                                                       </span>  '  + 
 '                                                   </td>  '  + 
 '                                                 '  + 
 '                                                   <td>  '  + 
 '                                                       <div class="table-data-feature">  '  + 
 '                                                           <button class="item" data-toggle="tooltip" data-placement="top" title="Send">  '  + 
 '                                                               <i class="zmdi zmdi-mail-send"></i>  '  + 
 '                                                           </button>  '  + 
 '                                                           <button class="item" data-toggle="modal" data-target="#mediumModal" data-placement="top" title="Edit" value="edit/'+data.data[i].id+'">  '  + 
 '                                                               <i class="zmdi zmdi-edit"></i>  '  + 
 '                                                           </button>  '  + 
 '                                                           <button class="item" data-toggle="tooltip" data-placement="top" title="Delete" value="delete/'+data.data[i].id+'">  '  + 
 '                                                               <i class="zmdi zmdi-delete"></i>  '  + 
 '                                                           </button>  '  + 
 '                                                           <button class="item" data-toggle="tooltip" data-placement="top" title="More">  '  + 
 '                                                               <i class="zmdi zmdi-more"></i>  '  + 
 '                                                           </button>  '  + 
 '                                                       </div>  '  + 
 '                                                   </td>  '  + 
 '                                               </tr>  '  + 
 '                                              <tr class="spacer"></tr>  '  );
                                        }


                                    
                                }
                                else {

                                   
                          
                                }
                            }
                        });
      
                        

                    });
                           
                
                </script>




    @stop